module.exports.notFound = (req, res, next) => {
    return res.status(404).json({
        status: false,
        message: "Resource not found!",
    });
};
