const express = require("express");
const connectDB = require("./db/connect");
const dotenv = require("dotenv");
const { notFound } = require("./middleware/notFound");
const { errorHandler } = require("./middleware/error-handler");

dotenv.config();
const app = express();

app.use(express.json());
app.use("/", require("./routes"));
app.use(notFound);
app.use(errorHandler);

const PORT = process.env.PORT || 3000;

const start = async () => {
    await connectDB(process.env.MONGO_URI);
    app.listen(PORT, (err) => {
        if (err) {
            console.log("Error : ", err);
            return;
        }
        console.log(`Server is listening on port : ${PORT}`);
    });
};

start();
