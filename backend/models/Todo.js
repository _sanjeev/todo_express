const mongoose = require("mongoose");

const TodoSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Name is mandatory!"],
        maxLength: [20, "Name can't be greater than 20"],
        trim: true,
    },
    completed: {
        type: Boolean,
        default: false,
    },
});

module.exports = mongoose.model("Todo", TodoSchema);
