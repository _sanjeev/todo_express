class CustomApiError extends Error {
    constructor(message, statusCode, status) {
        super(message);
        this.statusCode = statusCode;
        this.status = status;
    }
}

const createCustomError = (message, statusCode, status) => {
    return new CustomApiError(message, statusCode, status);
};

module.exports = { createCustomError, CustomApiError };
