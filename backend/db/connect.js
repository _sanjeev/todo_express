const mongoose = require("mongoose");

mongoose.set("strictQuery", true);
const connectDB = (url) => {
    return mongoose
        .connect(url)
        .then(() => console.log("connected to the database"))
        .catch((err) => console.log("error", err));
};

module.exports = connectDB;
