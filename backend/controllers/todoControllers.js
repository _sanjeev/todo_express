const Todo = require("./../models/Todo");
const { asyncWrapper } = require("../middleware/asyncWrapper");
const { createCustomError } = require("../errors/custom-errors");

module.exports.createTodo = asyncWrapper(async (req, res) => {
    const { name, completed } = req.body;
    const todo = await Todo.create({ name, completed });
    return res.status(200).json({
        status: true,
        message: "Successfully create the todo!",
        entity: todo,
    });
});

module.exports.getAllTodo = asyncWrapper(async (req, res) => {
    const allTodos = await Todo.find({});
    return res.status(200).json({
        status: true,
        message: "Successfully get all todos!",
        entity: allTodos,
    });
});

module.exports.getTodo = asyncWrapper(async (req, res, next) => {
    const { id: todoId } = req.params;
    const todo = await Todo.findOne({
        _id: todoId,
    });
    if (!todo) {
        return next(createCustomError("Successfully get the todo!", 404, true));
        return res.status(404).json({
            status: true,
            message: "Successfully get the todo!",
            entity: null,
        });
    }
    return res.status(200).json({
        status: true,
        message: "Successfully get the todo!",
        entity: todo,
    });
});

module.exports.updateTodo = asyncWrapper(async (req, res) => {
    const { id: todoId } = req.params;
    const { name } = req.body;
    const todo = await Todo.findOneAndUpdate(
        {
            _id: todoId,
        },
        {
            name: name,
        },
        {
            new: true,
            runValidators: true,
        }
    );
    if (!todo) return next(createCustomError("Successfully get the todo!", 404, true));
    // return res.status(404).json({
    //     status: true,
    //     message: "Successfully update the todo!",
    //     entity: null,
    // });
    return res.status(200).json({
        status: true,
        message: "Successfully update the todo!",
        entity: todo,
    });
});

module.exports.deleteTodo = asyncWrapper(async (req, res) => {
    const { id: todoId } = req.params;
    const todo = await Todo.findOneAndDelete({
        _id: todoId,
    });
    if (!todo) {
        return next(createCustomError("Successfully get the todo!", 404, true));
        return res.status(404).json({
            status: true,
            message: "Successfully delete the todo!",
            entity: null,
        });
    }
    return res.status(200).json({
        status: true,
        message: "Successfully delete the todo",
        entity: todo,
    });
});
