const express = require("express");
const { createTodo, getAllTodo, getTodo, updateTodo, deleteTodo } = require("../controllers/todoControllers");
const router = express.Router();

router.post("/todo", createTodo);
router.get("/todos", getAllTodo);
router.get("/todo/:id", getTodo);
router.patch("/todo/:id", updateTodo);
router.delete("/todo/:id", deleteTodo);

module.exports = router;
