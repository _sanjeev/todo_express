import axios from "axios";

export const getWithOutAuth = (url) => {
    try {
        return new Promise((resolve, reject) => {
            axios
                .get(url)
                .then((response) => {
                    if (response && response.data) resolve(response.data);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    } catch (error) {
        console.error(url, { error: JSON.stringify(error) });
    }
};

export const postWithOutAuth = (url, entity) => {
    try {
        return new Promise((resolve, reject) => {
            axios
                .post(url, entity)
                .then((response) => resolve(response))
                .catch((error) => reject(error));
        });
    } catch (error) {
        console.error(url, { error: JSON.stringify(error) });
    }
};

export const updateWithOutAuth = (url, entity) => {
    try {
        return new Promise((resolve, reject) => {
            axios
                .patch(url, entity)
                .then((response) => {
                    resolve(response);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    } catch (error) {
        console.log(url, { error: JSON.stringify(error) });
    }
};

export const deleteWithOutAuth = (url) => {
    try {
        return new Promise((resolve, reject) => {
            axios
                .delete(url)
                .then((response) => {
                    resolve(response);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    } catch (error) {
        console.error(url, { error: JSON.stringify(error) });
    }
};
