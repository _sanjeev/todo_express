import { getAllTodosUrl } from "./urls/todos.url";
import { getWithOutAuth } from "./http-service";

export const getAllTodos = () => {
    const url = getAllTodosUrl();
    return getWithOutAuth("http://localhost:6000/api/v1/todos");
};
