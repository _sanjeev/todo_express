const ApiUrl = process.env.REACT_APP_API_URL;

export const UrlParamsReplace = (url, params = {}) => {
    let urlWithPrefix = `${ApiUrl}${url}`;
    if (params) {
        Object.keys(params).forEach((key) => {
            urlWithPrefix = urlWithPrefix.replace(`:${key}`, params[key]);
        });
    }
    return urlWithPrefix;
};
