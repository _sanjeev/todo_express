import { getAllTodos } from "./services/todos-service";
import { useEffect } from "react";
import axios from "axios";
function App() {
    useEffect(() => {
        getTodos();
    }, []);

    const getTodos = async () => {
        // const response = await getAllTodos();
        // console.log("🚀 ~ file: App.js:6 ~ useEffect ~ response", response);
        const response = axios
            .get("http://localhost:6000/api/v1/todos")
            .then((response) => {
                console.log(response.data);
            })
            .catch((error) => {
                console.log(error);
            });
    };

    return <div>hello</div>;
}

export default App;
